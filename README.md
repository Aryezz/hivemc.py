# HiveMCpy
This is an inofficial API Wrapper of the [HiveMC API](https://apidoc.hivemc.com/)

I started this project mainly to learn more about python class structure and Library design and also to learn how to make a code documentation with sphinx, if you have any suggestions, please let me know :)
